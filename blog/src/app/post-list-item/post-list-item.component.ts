import { Component, Input, OnInit } from '@angular/core';
import {PostService} from '../post.service';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {

  @Input() title: string;
  @Input() content: string;
  @Input() loveIt: number;
  @Input() created_at: Date;
  @Input() index: number;
  // posts: any[];

  constructor(private postService: PostService) {
    this.created_at = new Date();
  }

  ngOnInit() {
    // this.posts = this.postService.posts;
  }

  toLoveIt(index) {
    this.postService.loveIt(index);
  }

  dontLoveIt(index) {
    this.postService.dontLoveIt(index);
  }
}
