import { Injectable } from '@angular/core';
import {Post} from './Post';

@Injectable()
export class PostService {

  posts: any[] = [
    {
      title: 'Post',
      content: `Lorem ipsum dolor sit amet,  et  magna aliqua.`,
      loveIt: 0,
      created_at: new Date(),
      index: 1,
    },
    {
      title: 'Post',
      content: `olor sit amet,  et  magna aliqua.`,
      loveIt: 0,
      created_at: new Date(),
      index: 2
    },
    {
      title: 'Post',
      content: `Lorem ia aliqua.`,
      loveIt: 0,
      created_at: new Date(),
      index: 3,
    }
  ];

  constructor() { }

  loveIt(index) {
    this.posts[index].loveIt++;
  }

  dontLoveIt(index) {
    this.posts[index].loveIt--;
  }
}
