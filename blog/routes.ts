import {Routes} from '@angular/router';
import {PostListComponent} from './src/app/post-list/post-list.component';
import {PostListItemComponent} from './src/app/post-list-item/post-list-item.component';

export const appRoutes: Routes = [
  {path: 'posts', component: PostListComponent},
  {path: '', component: PostListComponent},
  // {path: 'new', component: },
  {path: 'posts/id', component: PostListItemComponent},
];
